import pandas as pd
import os

directory = os.fsencode('/home/sunbeam/Documents/forproject/IMDB_SENTIMENT/outsentiments/')

outSentScoreDict = {'movie': [], 'sentiment score': []}

for file in os.listdir(directory):
    filenamecsv = os.fsdecode(file)
    filename = filenamecsv.replace('.csv', '')
    sentimentdf = pd.read_csv(os.path.join('/home/sunbeam/Documents/forproject/IMDB_SENTIMENT/outsentiments/', filenamecsv))
    sentDist = sentimentdf['sentiment'].value_counts()
    pos = sentDist[1]
    neg = sentDist[0]
    sentScore = (pos/(pos+neg)) * 100
    outSentScoreDict['movie'].append(filename)
    outSentScoreDict['sentiment score'].append(sentScore)
sentScoredf = pd.DataFrame.from_dict(outSentScoreDict)
# sentScoredf.to_csv(os.path.join('/home/sunbeam/Documents/forproject/IMDB_SENTIMENT/sentscore/', 'SentimentScoreDistribution.csv'))

import sqlalchemy
database_username = 'sunbeam'
database_password = 'sunbeam'
database_ip       = 'localhost'
database_name     = 'sunbeam'
database_connection = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                               format(database_username, database_password, 
                                                      database_ip, database_name))
sentScoredf.to_sql(con=database_connection, name='sentimentscore', if_exists='replace')
